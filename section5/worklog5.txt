---------------------------------------
Create new virtual environment (same way):
---------------------------------------
$ virtualenv .venv --python=python3.8.1
$ python3 -m venv .venv

---------------------------------------
Activate existing virtual environment:
---------------------------------------
MacBook-Pro:rest-api-sections zollak$ pwd
/Users/zollak/Documents/Job/Citi/udemy/Python/rest-api-sections
MacBook-Pro:rest-api-sections zollak$ source .venv/bin/activate

---------------------------------------
Install modules:
---------------------------------------
(.venv) MacBook-Pro:section5 zollak$ pip install Flask
(.venv) MacBook-Pro:section5 zollak$ pip install Flask-RESTful
(.venv) MacBook-Pro:section5 zollak$ pip install Flask-JWT
(.venv) MacBook-Pro:section5 zollak$ pip freeze
aniso8601==8.0.0
astroid==2.3.3
Click==7.0
Flask==1.1.1
Flask-JWT==0.3.2
Flask-RESTful==0.3.8
isort==4.3.21
itsdangerous==1.1.0
Jinja2==2.11.1
lazy-object-proxy==1.4.3
MarkupSafe==1.1.1
mccabe==0.6.1
PyJWT==1.4.2
pylint==2.4.4
pytz==2019.3
six==1.14.0
Werkzeug==1.0.0
wrapt==1.11.2


---------------------------------------
Deactivate environment
---------------------------------------
(.venv) MacBook-Pro:section5 zollak$ deactivate

---------------------------------------
Basic sqlite3 commands:
---------------------------------------
(.venv) MacBook-Pro:test zollak$ python3 -u "/Users/zollak/Documents/Job/Citi/udemy/Python/rest-api-sections/section5/test/test.py"
(1, 'jose', 'asdf')
(2, 'rolf', 'asdf')
(3, 'anne', 'xyz')

(.venv) MacBook-Pro:test zollak$ sqlite3 test_data.db
SQLite version 3.19.4 2017-08-18 19:28:12
Enter ".help" for usage hints.
sqlite> .tables 
users
sqlite> select * from users;
1|jose|asdf
2|rolf|asdf
3|anne|xyz
sqlite> .dump users
PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE users (id int, username text, password text);
INSERT INTO users VALUES(1,'jose','asdf');
INSERT INTO users VALUES(2,'rolf','asdf');
INSERT INTO users VALUES(3,'anne','xyz');
COMMIT;
sqlite> .quit


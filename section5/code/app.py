from flask import Flask
from flask_restful import Api
from flask_jwt import JWT
from datetime import timedelta
from security import authenticate, identity
from user import UserRegister
from item import Item, ItemList

app = Flask(__name__)
app.secret_key = 'jose'
app.config['JWT_AUTH_URL_RULE'] = '/login' # /login endpoint
app.config['JWT_EXPIRATION_DELTA'] = timedelta(seconds=1800) # config JWT to expire within half an hour
api = Api(app)

jwt = JWT(app, authenticate, identity) # default is /auth, however app.config['JWT_AUTH_URL_RULE'] set another endpoint

# items = [] # no longer needed in memory items... we're going to use sqlite db for it 

api.add_resource(Item, '/item/<string:name>')
api.add_resource(ItemList, '/items')
api.add_resource(UserRegister, '/register')

if __name__ == '__main__':
    app.run(port=5000, debug=True)
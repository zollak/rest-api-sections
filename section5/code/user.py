import sqlite3
from flask_restful import Resource, reqparse

class User:

    def __init__(self, _id, username, password):
        self.id = _id
        self.username = username
        self.password = password
    
    # def find_by_username(self, username):
    #     connection = sqlite3.connect('../test/test_data.db')
    #     cursor = connection.cursor()
    #     query = "SELECT * FROM users WHERE username=?"
    #     result = cursor.execute(query, (username,)) # query parameter should be tuple format!!!
    #     row = result.fetchone()
    #     # if row is not None:  #same like "if row:"
    #     if row:
    #         user = User(row[0], row[1], row[2])
    #     else:
    #         user = None
        
    #     connection.close()
    #     return user

    @classmethod
    def find_by_username(cls, username): #change self to cls for classmethod
        connection = sqlite3.connect('my_sqlite3.db')
        cursor = connection.cursor()
        query = "SELECT * FROM users WHERE username=?"
        result = cursor.execute(query, (username,)) # query parameter should be tuple format!!!
        row = result.fetchone()
        # if row is not None:  #same like "if row:"
        if row:
            user = cls(*row) #change User to cls for classmethod, instead of setting arguments "row[0], row[1], row[2]" use *row
        else:
            user = None
        
        connection.close()
        return user

    @classmethod
    def find_by_id(cls, _id):
        connection = sqlite3.connect('my_sqlite3.db')
        cursor = connection.cursor()
        query = "SELECT * FROM users WHERE id=?"
        result = cursor.execute(query, (_id,))
        row = result.fetchone()
        if row:
            userid = cls(*row)
        else:
            userid = None
        
        connection.close()
        return userid

class UserRegister(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('username',
        type=str,
        required=True,
        help="This field cannot be blank!"
    )
    parser.add_argument('password',
        type=str,
        required=True,
        help="This field cannot be blank!"
    )

    def post(self):
        data = UserRegister.parser.parse_args()

        if User.find_by_username(data['username']):
            return {"message": "User already exists."}, 400

        connection = sqlite3.connect('my_sqlite3.db')
        cursor = connection.cursor()
        # print(f"username: {data['username']}")
        # print(f"password: {data['password']}")
        
        query = "INSERT INTO users VALUES (NULL, ?, ?)"
        cursor.execute(query, (data['username'], data['password']))

        connection.commit()
        connection.close()

        return {"message": "User created sccessfully."}, 201

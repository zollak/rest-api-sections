import sqlite3

# create db
connection = sqlite3.connect('test_data.db') # connection string
cursor = connection.cursor()
create_table = "CREATE TABLE users (id int, username text, password text)" # schema: how the data is going to look
cursor.execute(create_table) # query

# store some data in the db
user = (1, "jose", "asdf")
inser_query = "INSERT INTO users VALUES (?, ?, ?)"
cursor.execute(inser_query, user)


# insert more users into db
users = [
    (2, "rolf", "asdf"),
    (3, "anne", "xyz")
]
cursor.executemany(inser_query, users)
select_query = "SELECT * FROM users"
for row in cursor.execute(select_query):
    print(row)

# at the end of db operations we should ALWAYS save and close connection!!!
connection.commit() # save changes 
connection.close()

# You should remove test_data.db if you want to run again this test.py script
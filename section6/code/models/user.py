import sqlite3
from db import db

class UserModel(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80))
    password = db.Column(db.String(80))

    def __init__(self, username, password):
        # self.id = _id
        self.username = username
        self.password = password
    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    @classmethod
    def find_by_username(cls, username): #change self to cls for classmethod
        # connection = sqlite3.connect('my_sqlite3.db')
        # cursor = connection.cursor()
        # query = "SELECT * FROM users WHERE username=?"
        # result = cursor.execute(query, (username,)) # query parameter should be tuple format!!!
        # row = result.fetchone()
        # if row:
        #     user = cls(*row) #change User to cls for classmethod, instead of setting arguments "row[0], row[1], row[2]" use *row
        # else:
        #     user = None
        
        # connection.close()
        # return user
        # return cls.query # SELECT * from users
        return cls.query.filter_by(username=username).first() # SELECT * from users WHERE username=username LIMIT 1 

    @classmethod
    def find_by_id(cls, _id):
        # connection = sqlite3.connect('my_sqlite3.db')
        # cursor = connection.cursor()
        # query = "SELECT * FROM users WHERE id=?"
        # result = cursor.execute(query, (_id,))
        # row = result.fetchone()
        # if row:
        #     userid = cls(*row)
        # else:
        #     userid = None
        
        # connection.close()
        # return userid
        return cls.query.filter_by(id=_id).first()